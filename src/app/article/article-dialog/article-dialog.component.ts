import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Article } from '../article.interface';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { EditorModule } from 'primeng/editor';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { MessagesModule } from 'primeng/messages';
import moment from 'moment';

@Component({
  selector: 'app-article-dialog',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DialogModule,
    EditorModule,
    ButtonModule,
    InputTextModule,
    CalendarModule,
    MessagesModule,
  ],
  templateUrl: './article-dialog.component.html',
  styleUrls: ['./article-dialog.component.scss'],
})
export class ArticleDialogComponent {
  @Input() dialogVisible = false;

  @Output() dialogVisibleChange = new EventEmitter();

  @Output() submitArticle: EventEmitter<Article> = new EventEmitter<Article>();

  insertForm: FormGroup = new FormGroup({
    id: new FormControl(),
    title: new FormControl('', Validators.required),
    href: new FormControl('', Validators.required),
    date: new FormControl(moment().format('yyyy/MM/DD')),
    author: new FormControl('', Validators.required),
    category: new FormControl('', Validators.required),
    categoryLink: new FormControl('', Validators.required),
    summary: new FormControl('', Validators.required),
  });

  isFormInValid = false;

  doSubmitArticle() {
    this.isFormInValid = this.insertForm.status === 'INVALID';
    if (this.isFormInValid) return;
    this.submitArticle.emit(this.insertForm.value as Article);
  }

  doHiddenDialog() {
    this.dialogVisible = false;
    this.dialogVisibleChange.emit(this.dialogVisible);
  }
}
