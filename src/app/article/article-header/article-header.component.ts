import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Article } from '../article.interface';
import { NewTitle } from '../article.interface';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'app-article-header',
  standalone: true,
  imports: [CommonModule, FormsModule, ButtonModule],
  templateUrl: './article-header.component.html',
  styleUrls: ['./article-header.component.scss'],
})
export class ArticleHeaderComponent implements OnInit {
  @Input() article!: Article;

  @Output() delete: EventEmitter<Article> = new EventEmitter<Article>();

  @Output() titleChanged: EventEmitter<NewTitle> = new EventEmitter<NewTitle>();

  @Output() openDialog: EventEmitter<Article> = new EventEmitter<Article>();

  isDialogOpened = false;

  isEdit = false;

  origItem!: Article;

  articlePartOfInfo!: NewTitle;

  ngOnInit(): void {
    this.origItem = { ...this.article };
  }

  doDeleteArticle() {
    this.delete.emit(this.article);
  }

  doEditTitle() {
    this.articlePartOfInfo = {
      id: this.article.id,
      title: this.article.title,
    };
    this.isEdit = false;
    this.titleChanged.emit(this.articlePartOfInfo);
  }

  onCancelTitle() {
    this.article = { ...this.origItem };
    this.isEdit = false;
  }
}
