import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Article } from '../article.interface';

@Component({
  selector: 'app-article-footer',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './article-footer.component.html',
  styleUrls: ['./article-footer.component.scss'],
})
export class ArticleFooterComponent {
  @Input() article!: Article;
}
