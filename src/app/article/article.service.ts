import { Injectable, inject } from '@angular/core';
import { Article } from './article.interface';
import { NewTitle, ArticleList } from './article.interface';
import { HttpClient } from '@angular/common/http';
import { lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ArticleService implements ArticleList {
  #http = inject(HttpClient);

  #url = `http://localhost:3000/articles`;

  getArticles = async () => {
    const getArticlesObservable$ = this.#http.get<Article[]>(`${this.#url}`);
    return await lastValueFrom(getArticlesObservable$);
  };

  removeArticle = async (articleId: number) => {
    const deleteObservable$ = this.#http.delete(`${this.#url}/${articleId}`);
    return await lastValueFrom(deleteObservable$);
  };

  modifyArticle = async (newTitle: NewTitle) => {
    const modifyObservable$ = this.#http.patch(
      `${this.#url}/${newTitle.id}`,
      newTitle,
    );
    return await lastValueFrom(modifyObservable$);
  };

  insertArticle = async (article: Article) => {
    const insertObserveble$ = this.#http.post(`${this.#url}`, article);
    return await lastValueFrom(insertObserveble$);
  };
}
