import { Article } from '../article.interface';
import { ArticleService } from '../article.service';
import { Component, inject, signal, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ArticleHeaderComponent } from '../article-header/article-header.component';
import { ArticleFooterComponent } from '../article-footer/article-footer.component';
import { ArticleDialogComponent } from '../article-dialog/article-dialog.component';
import { NewTitle } from '../article.interface';
import { HttpClientModule } from '@angular/common/http';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'app-article',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ArticleHeaderComponent,
    ArticleFooterComponent,
    ArticleDialogComponent,
    HttpClientModule,
    ButtonModule,
  ],
  providers: [ArticleService],
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
})
export class ArticleComponent implements OnInit {
  articleService: ArticleService = inject(ArticleService);

  articles = signal([] as Article[]);

  async ngOnInit() {
    try {
      this.articles.set(await this.articleService.getArticles());
    } catch (error) {
      console.error(`getArticles error : ${error}`);
    }
  }
  async onModifyArticleTitle(newTitle: NewTitle) {
    try {
      await this.articleService.modifyArticle(newTitle);
    } catch (error) {
      console.error(`onModifyArticle error : ${error}`);
    }
  }

  async onDeleteArticle(article: Article) {
    try {
      await this.articleService.removeArticle(article.id);
    } catch (error) {
      console.error(`onDeleteArticle error : ${error}`);
    }
  }
}
