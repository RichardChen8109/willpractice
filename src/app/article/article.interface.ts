export interface Article {
  id: number;
  title: string;
  href: string;
  date: string;
  author: string;
  category: string;
  categoryLink: string;
  summary: string;
}

export interface NewTitle {
  id: number;
  title: string;
}

export interface ArticleList {
  modifyArticle(post: NewTitle): Promise<object>;
  removeArticle(articleID: number): Promise<object>;
  getArticles(): Promise<Article[]>;
}
