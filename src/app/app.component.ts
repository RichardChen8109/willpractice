import { Component, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ArticleComponent } from '../app/article/article-list/article.component';
import { Article } from './article/article.interface';
import { ArticleDialogComponent } from './article/article-dialog/article-dialog.component';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
import { ArticleService } from './article/article.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet,
    FormsModule,
    ArticleDialogComponent,
    HeaderComponent,
    FooterComponent,
    ArticleComponent,
    ButtonModule,
    ToastModule,
  ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ArticleService],
})
export class AppComponent {
  articleService = inject(ArticleService);

  keyword = signal<string>('');

  isDialogOpened = false;

  public keywordReset() {
    this.keyword.set('');
  }

  onOpenDialog() {
    this.isDialogOpened = true;
  }

  async onInsertArticleSubmit(article: Article) {
    try {
      await this.articleService.insertArticle(article);
    } catch (error) {
      console.error(`onInsertArticleSubmit error : ${error}`);
    }
  }
}
